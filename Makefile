#!/bin/bash

RED=\033[0;31m
GREEN=\033[0;32m
NOCOLOUR=\033[0m
PACK=null

.PHONY: all
all: 
	@printf "$(GREEN)usage:\n" ;
	@printf "$(GREEN)init: \t$(NOCOLOUR) initialise the app\n" ;
	@printf "$(GREEN)start: \t$(NOCOLOUR) starts the Docker app-ssl container \n" ;
	@printf "$(GREEN)stop: \t$(NOCOLOUR) stops the Docker app-ssl container \n" ;
	@printf "$(GREEN)create-cert: \t$(NOCOLOUR) creates SSL sertificates for app-ssl container \n" ;

.PHONY: init
init:
	docker build -t app .

.PHONY: start
start:
	docker run -t -d -p 443:443 -p 80:80 --rm -v $(PWD)/app:/var/www/app --name app-ssl app ; \
	printf "Use URL " ; \
	docker inspect --format '{{ .NetworkSettings.IPAddress }}' app-ssl ;

.PHONY: stop
stop:
	docker stop app-ssl

.PHONY: create-cert
create-cert:
	printf "Fill all in as you like exept 'Common Name' that must be 'app-ssl' \n" ; \
	openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./app/server.key -out ./app/server.crt

.PHONY: login
login:
	docker exec -it app-ssl bash

.PHONY: destroy
destroy:
	docker system prune
	docker volume prune

.PHONY: destroy-everything
destroy-everything:
	docker system prune -a -f
	docker volume prune -f
