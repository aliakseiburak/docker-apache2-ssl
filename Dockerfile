FROM ubuntu:latest


# Install apache and supplimentary programs:
# openssh-server, curl, and lynx-cur are for debugging the container.
RUN apt-get update
RUN apt-get -y upgrade

# The problem tzdata, which stops with interactive dialog.
ARG DEBIAN_FRONTEND="noninteractive"
ENV TZ=Europe/Minsk

RUN apt-get -y install apache2 \
    software-properties-common \
    tzdata


RUN add-apt-repository ppa:ondrej/php
RUN apt-get update

# The following additional packages will be installed:
#  libapache2-mod-php7.4 libargon2-0 libedit2 libpcre2-8-0 libsodium23
#  php-common php7.4-cli php7.4-common php7.4-json php7.4-opcache
#  php7.4-readline psmisc tzdata
RUN apt-get -y install php7.4

# Update the PHP.ini file, enable <? ?> tags and quieten logging.
RUN sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.4/apache2/php.ini
RUN sed -i "s/error_reporting = .*$/error_reporting = E_ERROR | E_WARNING | E_PARSE/" /etc/php/7.4/apache2/php.ini

# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

EXPOSE 80

# Copy this repo into place.
# https://stackoverflow.com/questions/41935435/understanding-volume-instruction-in-dockerfile
VOLUME ["/var/www", "/etc/apache2/sites-enabled"]
#VOLUME ["/var/www/app", "/etc/apache2/sites-enabled"]

# Update the default apache site with the config we created.
ADD apache-config.conf /etc/apache2/sites-enabled/000-default.conf
#ADD app/index.php /var/www/app

COPY ./app/server.crt /etc/apache2/ssl/server.crt
COPY ./app/server.key /etc/apache2/ssl/server.key

RUN a2enmod rewrite
RUN a2enmod ssl

# By default, simply start apache.
CMD /usr/sbin/apache2ctl -D FOREGROUND
