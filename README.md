# Docker with Apache Ubuntu PHP
Installing Docker You can click here [Docker Installation]("https://docs.docker.com/installation/#installation")

## Setting up the project
```
./Dockerfile
./app/index.php
./apache-config.conf
./Makefile
```

## Managing the container
Basic commands available. Also check Makefile itself as some may not be listed.
`make`

## Running the container
`make start`

## Logging into the container
`make login`

## Stop/Remove the container
```
make stop
make destroy
```

## Volume logs of container to host
`docker run -d -p 80:80 -v /data/server/app:/var/www/site/app -v /data/server/log:/var/log/apache2 --name app app`

## Sources:
https://medium.com/@nh3500/how-to-create-self-assigned-ssl-for-local-docker-based-lamp-dev-environment-on-macos-sierra-ab606a27ba8a
https://stackoverflow.com/questions/41935435/understanding-volume-instruction-in-dockerfile
https://serverfault.com/questions/949991/how-to-install-tzdata-on-a-ubuntu-docker-image
https://stackoverflow.com/questions/43692961/how-to-get-ip-address-of-running-docker-container/47226863
